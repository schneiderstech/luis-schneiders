// set up ======================================================================
// get all the tools we need
var express         = require('express');
var app             = express();
var port            = process.env.PORT || 80;
var bodyParser      = require('body-parser');
var passport        = require('passport');
var flash           = require('connect-flash');
var cookieParser    = require('cookie-parser');
var session         = require('express-session');
var compress        = require('compression');

app.use(bodyParser.urlencoded({ extended: true }));


// configuration ===============================================================
require('./config/database.js');
require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(cookieParser()); // read cookies (needed for auth)
app.use(compress());

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/bower_components'));
app.use(express.static(__dirname + '/app/admin'));

app.set('view engine', 'ejs'); // set up ejs for templating
// to render html files using Angular2
app.engine('html', require('ejs').renderFile);

// redirect http://www.nodejs.luischneiders.com to http://nodejs.luisschneiders.com
app.use(function(req, res, next) {
    if(/^www\./.test(req.headers.host)) {
        res.redirect(301, req.protocol + '://' + req.headers.host.replace(/^www\./,'') + req.url);
    }
    else{
        next();
    }
});
// required for passport
app.use(session({ 
    secret: 'odnanref_lu1s_01001010', 
    resave: true,
    saveUninitialized: true 
})); // session secret
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);

