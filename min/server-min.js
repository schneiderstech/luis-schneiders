// server.js
// load the things we need
var express = require('express');
var sm = require('sitemap');
var ejs = require('ejs');
var fs = require('fs');
var path = require('path');
var http = require('http');
var url = require('url');
var req = require('request');
var nodemailer = require('nodemailer');
var xoauth2 = require('xoauth2');
var compress = require('compression');
var bodyParser = require('body-parser');
var validator = require('email-validator');
var db = require('./models/db');


//var gulp = require('gulp');
//var sitemap = require('gulp-sitemap');
//var save = require('gulp-save');

var app = express();

app.use(compress());
app.use(express.static(__dirname + '/helpers'));
app.use(express.static(__dirname + '/models'));
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');

// redirect http://www.nodejs.luischneiders.com to http://nodejs.luisschneiders.com
app.use(function(req, res, next) {
    if(/^www\./.test(req.headers.host)) {
        res.redirect(301, req.protocol + '://' + req.headers.host.replace(/^www\./,'') + req.url);
    }
    else{
        next();
    }
});
//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}
// create sitemap
var mySite = 'http://local.developing:8080';
var sitemap = sm.createSitemap({
    hostname: mySite,
    cacheTime: 1000 * 60 * 24,        // keep sitemap cached for 24 hours
    urls: [
        { url: '/', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/index.ejs' },
        { url: '/about', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/about.ejs' },
        { url: '/get-in-touch', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/get-in-touch.ejs' },
        { url: '/projects', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/projects.ejs' },
        { url: '/blog', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/blog.ejs' }
    ]
});
//  read file header.json
var contentHeader = fs.readFileSync('models/header.json');
//  JSON parse    
contentHeader = JSON.parse(contentHeader);
// index page 
app.get('/', function (req, res) {
    'use strict';

    var contentIndex = fs.readFileSync('models/index.json');

    contentIndex = JSON.parse(contentIndex);
    res.render('pages/index', {
        contentHeader: contentHeader,
        contentIndex: contentIndex,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Node.js developer in Melbourne'
        }
    });
});
// about page 
app.get('/about', function (req, res) {
    'use strict';
    res.render('pages/about', {
        contentHeader: contentHeader,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Skills and work experience'
        }
    });
});
// projects page 
app.get('/projects', function (req, res) {
    'use strict';

    var contentProjects = fs.readFileSync('models/projects.json');

    contentProjects = JSON.parse(contentProjects);
    res.render('pages/projects', {
        contentHeader: contentHeader,
        contentProjects: contentProjects,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Check out some awesome projects'
        }
    });
});
// get in touch page 
app.get('/get-in-touch', function (req, res) {
    res.render('pages/get-in-touch', {
        contentHeader: contentHeader,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Get in touch'
        },
        emailConfig:
        {
            msg: 'All fields are required!',
            status : '',
            displayClass: 'hide'
        }
    });
});
// post contact form
app.post('/get-in-touch', function (req, res) {
    // honeypot for spambots
    if(req.body.company){
        res.render('pages/get-in-touch', {
            contentHeader: contentHeader,
            contentSEO:
            {
                mySite: getHostName(req, myPort).hostName,
                currentPage: req.path,
                title: 'Get in touch'
            },
            err: true,
            page: 'pages/get-in-touch',
            type: 'empty',
            name: req.body.name,
            email: req.body.email,
            subject: req.body.subject,
            message: req.body.message,
            emailConfig:
            {
                msg: 'Spambot detected. Contact us by email',
                status: '',
                displayClass: ''
            }

        });
        return;
    }
    // check if all fields are filled
    if(!req.body.name || !req.body.email || !req.body.subject || !req.body.message){
        // console.log('Im here');
        res.render('pages/get-in-touch', {
            contentHeader: contentHeader,
            contentSEO:
            {
                mySite: getHostName(req, myPort).hostName,
                currentPage: req.path,
                title: 'Get in touch'
            },
            err: true,
            page: 'pages/get-in-touch',
            type: 'empty',
            name: req.body.name,
            email: req.body.email,
            subject: req.body.subject,
            message: req.body.message,
            emailConfig:
            {
                msg: 'All Fields are required!',
                status: '',
                displayClass: ''
            }
        });
        return;
    }
    // check for valid email
    var email_check = validator.validate(req.body.email);
    if(!email_check) {
        console.log('Wrong email format!');
        res.render('pages/get-in-touch', {
            contentHeader: contentHeader,
            contentSEO:
            {
                mySite: getHostName(req, myPort).hostName,
                currentPage: req.path,
                title: 'Get in touch'
            },
            err: true,
            page: 'pages/get-in-touch',
            type: 'empty',
            name: req.body.name,
            email: req.body.email,
            subject: req.body.subject,
            message: req.body.message,
            emailConfig:
            {
                msg: 'Email is incorrect!',
                status: '',
                displayClass: ''
            }
        });
        return;
    }
    var transporter = nodemailer.createTransport('direct:?name=hostname');
    // var transporter = nodemailer.createTransport({
    //     service: 'gmail',
    //     auth: {
    //         xoauth2: xoauth2.createXOAuth2Generator({
    //             user: 'Web client Contact Form',
    //             clientId: '578289351272-egp54vb8fsep0sfc6oj4mdkoeqq1b0iu.apps.googleusercontent.com',
    //             clientSecret: 'htJkG6midAaZ67WMRWsCRCGl',
    //             refreshToken: '1/X2rn3BbS4WtVNY0QJige2B0aE5Zf586LwzGAOLP6qBU',
    //             accessToken: 'ya29.egLaCmNVY6piyDqkK9w6X_Y6agRfRzIcZ-SOWDtpHNkA8u0Bzr4T3yV8Yfn0tqUjGIOB' // optional 
    //         })
    //     }
    // });

    // Fill mail options
    var mailOptions = {
        from: req.body.name + ' 👥 <' + req.body.email + '>', // sender address
        to: 'schneiders.tech@gmail.com',
        subject: 'Contact via nodejs website', // Subject line
        text: 'Contact via nodejs website', // plaintext body
        html: 'From: <b>'+ req.body.name + '</b><br> <p>Subject: <b>' + req.body.message + '</b></p>'
    };
    transporter.sendMail(mailOptions, function(error, info){
        // email not sent
        if(error){
            console.log('Error is: ', error);
            res.render('pages/get-in-touch', {
                contentHeader: contentHeader,
                contentSEO:
                {
                    mySite: getHostName(req, myPort).hostName,
                    currentPage: req.path,
                    title: 'Get in touch'
                },
                err: true,
                page: 'pages/get-in-touch',
                type: 'error',
                emailConfig:
                {
                    msg : 'Error sending email, please try again!',
                    status: '',
                    displayClass: ''
                }
            });
        }else{
            res.render('pages/get-in-touch', {
                contentHeader: contentHeader,
                contentSEO:
                {
                    mySite: getHostName(req, myPort).hostName,
                    currentPage: req.path,
                    title: 'Get in touch'
                },
                err: true,
                page: 'pages/get-in-touch',
                type: 'success',
                emailConfig:
                {
                    msg : '',
                    status: 'Email sent successfuly!',
                    displayClass: ''
                }
            });
        }
    });
});
// blog page 
app.get('/blog', function (req, res) {
    'use strict';
//  connecting to mongodb    
    var listPosts = require('./models/posts');

    // listPosts.listAllPosts();
    // console.log('wow:', listPosts.listAllPosts.lfs_posts);
    res.render('pages/blog', {
        contentHeader: contentHeader,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Blog'
        },
        listAllPostsPublish: ''
    });
});
// sitemap page
app.get('/sitemap.xml', function (req, res) {
    'use strict';
    sitemap.toXML(function (err, xml) {
        if (err) {
            return res.status(500).end();
        }
        res.header('Content-Type', 'application/xml');
        res.send(xml);
    });
});
// robots.txt
app.get('/robots.txt', function(req, res) {
    res.setHeader('Content-Type', 'text/txt');
    res.sendFile(path.join(__dirname, '/views/pages/robots.txt'));
});
// Handle 404
app.use(function (req, res) {
    'use strict';
    res.status(404);
    res.render('pages/404.ejs', {
        title: '404: File Not Found',
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: '/',
            title: 'Node.js developer in Melbourne'
        }
    });
});
// Handle 500
app.use(function (error, req, res) {
    'use strict';
    res.status(500);
    res.render('pages/500.ejs', {
        title: '500: Internal Server Error',
        error: error,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: '/',
            title: 'Node.js developer in Melbourne'
        }
        
    });
});
var myPort = 8080;
app.listen(myPort);
//console.log('Port:', myPort, ' - We are all good to go!');


