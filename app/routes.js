// app/routes.js
module.exports = function(app, passport) {

    // this must be in the top
    app.use(require('./controllers/sitemap'));
    app.use(require('./controllers/robots'));
    // this must be in the top

    // home page controllers
    app.use(require('./controllers/home-page'));

    // about page controllers
    app.use(require('./controllers/about'));

    // projects page controllers
    app.use(require('./controllers/projects'));

    // get in touch page controllers
    app.use(require('./controllers/get-in-touch'));

    // get language controllers
    app.use(require('./controllers/language'));

    // login page controllers
    app.use(require('./controllers/login'));    

    // main dashboard page controllers
    app.use(require('./controllers/new-password')); 

    // main dashboard page controllers
    app.use(require('./controllers/main')); 

    // posts controllers
    app.use(require('./controllers/posts'));

    // users controllers
    app.use(require('./controllers/users'));

    // this must be in the bottom
    app.use(require('./controllers/404'));
    app.use(require('./controllers/500'));
    // this must be in the bottom
};