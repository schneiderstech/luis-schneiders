<script>
function createModal(img, alt, description, link, link_description){
    $("#modal").empty();
    $("#modal").append(
        '<div class="row">'+
        '<div class="large-8 large-centered columns">'+
        '<button class="close-button" data-close aria-label="Close reveal" type="button">'+
        '<span aria-hidden="true">&times;</span>'+
        '</button>'+
        '<div class="small-12 columns">'+
        '<h1 class="text-uppercase">'+alt+'</h1>'+
        '<img src="'+img+'" alt="'+alt+'">'+
        '</div>'+
        '<div class="small-12 columns">'+
        '<p>'+description+'<a href="'+link+'" target="_blank">'+link_description+'</p>'+
        '</div>'+
        '</div>'+
        '</div>'
    );
}
</script>