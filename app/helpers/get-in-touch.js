<script>
    $(function(){
        $("#typed-idea").typed({
            strings: ["a project", "an idea", "a dream"],
            typeSpeed: 70,
            backDelay: 2500,
            loop: true,
            contentType: 'html', // or text
            // defaults to false for infinite loop
            loopCount: false,
            resetCallback: function() { newTyped(); }
        });
        $(".reset").click(function(){
            $("#typed-idea").typed('reset');
        });
    });
    function newTyped(){ /* A new typed object */ }
</script>