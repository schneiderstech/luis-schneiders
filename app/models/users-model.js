var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
    
var Schema = mongoose.Schema;
    // ObjectId = Schema.ObjectId;

var usersSchema = new Schema ({ 
    user_name: { type: String, required: true, index: { unique: true } }, 
    user_first_name: { type: String, require: false }, 
    user_middle_name: { type: String, require: false }, 
    user_last_name: { type: String, require: false }, 
    user_email: { type: String, required: true }, 
    user_password: { type: String, required: true },
    user_role: { type: String, required: true }, 
    user_registered_date: { type : Date, default: Date.now },
    user_status: { type: String, required: true }
});

// methods ======================

// generating a hash
usersSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};

// checking if password is valid
usersSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.user_password);
};

module.exports = mongoose.model('lfs_users', usersSchema);