// var express         = require('express');
// var passport        = require('passport');
var express         = require('express');
var router          = express();

var nodemailer      = require('nodemailer');
var validator       = require('email-validator');
var fs              = require('fs');

router.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

// get USERS model
var UsersModel = require ('../models/users-model');

module.exports.uploadProfile = function(req, res) {
    console.log('Image is >', req.files.user_image);
    if(err) 
        return res.json(err);
    return res.json(done);
    // fs.readFile(req.files.user_image.path, function (err, data) {
    //     var newPath = './uploads/' + req.files.user_image.filename;
    //     fs.writeFile(newPath, data, function (err) {
    //         if(err){
    //             return err;
    //         }
    //     });
    // });
    // console.log('File..:', req.file);
};

//  check if USER collection has data then redirect it to login page 
module.exports.displayRegisterPage = function(req, res) {
    UsersModel
    .find()
    .exec(function(err, checkUserCollection) {
        if (err) {
            return err;
        } else {
            if(!checkUserCollection.length) {
                res.render('admin/register', {
                    title: 'Register',
                    message : req.flash('signupMessage')
                });
            } else {
                res.render('admin/login', {
                    title   : 'Login',
                    message : req.flash('loginMessage')
                });
            }
        }
    });    
};

// list all USERS in admin area
module.exports.listAllUsers = function(req, res) {
    UsersModel
    .find({ 'user_status' : { $ne : 'deleted' }})
    .sort({ 'user_name' : 'descending' })
    .exec(function(err, listallusers) {
        if (err) {
            return err;
        } else {
            res.json(listallusers);
        }
    });
};

// get user by ID
module.exports.getUserById = function(req, res) {
    // console.log('get user session: ', req.session);
    // get ID selected 
    UsersModel
    .findById({ _id : req.params.id }, function(err, done){        
        if(err) 
            return res.json(err);
        return res.json(done);
    })
    .where('user_status').ne('deleted');
};

 // add new user
module.exports.addNewUser = function(req, res) {
    var newUser = new UsersModel();
    var password = req.body.username + '_1234';
    var emailPassword = password;
    // set the user's local credentials
    newUser.user_name           = req.body.username;
    newUser.user_first_name     = '';
    newUser.user_middle_name    = '';
    newUser.user_last_name      = '';
    newUser.user_email          = req.body.email;
    newUser.user_password       = newUser.generateHash(password); // use the generateHash function in our user model
    newUser.user_role           = req.body.role;
    newUser.user_status         = req.body.status;

    // save the user
    newUser.save(function (err, done) {
        if (err)
            return res.json(err);

        //  send email invitation
        var adminEmail  = 'luis.schneiders@gmail.com';
//================================================================== 
        var email_check = validator.validate(req.body.email);
        if(!email_check) {
            // console.log('Wrong email format!');
            return;
        }
        var transporter = nodemailer.createTransport('direct:?name=hostname');
        // Fill mail options
        var mailOptions = {
            from        : adminEmail, // sender address
            to          : req.body.email,
            subject     : 'Invitation to collaborate in our website', // Subject line
            text        : 'Invitation to collaborate in our website', // plaintext body
            html        : 'Hi '+ req.body.username + ', <br> <p>We are pleased to inform that you have'
                          + ' been invited to collaborate in editing and posting in our website.<br>'
                          + 'Bellow is your credentials to login in the portal:<br><br>'
                          + 'Username: ' + req.body.username + '<br>'
                          + 'Password: ' + emailPassword + '<br>'
                          + '<p>Reminder:<br>After your first login, your will be able to choose a password of your choice.</p><br>'
                          + 'Kind regards, <br><br>'
                          + 'Team - Admin'
        };
        transporter.sendMail(mailOptions, function(error){
            // email not sent
            if(error)
                // console.log('Error is: ', error);
                return error;
        });
//================================================================== 
        return res.json(done);
    });
};

// update user status
module.exports.updateUserStatus = function(req, res) {
    UsersModel
    .update( { _id : req.body.id} , { $set: { user_status : req.body.status }}, function(err, results){
        if(err) {
            return res.json(err);
        } 
        return res.json(results);
    });
};

// update user details
module.exports.updateUserDetails = function(req, res) {
    // console.log('List ID:', req.body._id);
    // save the user
    UsersModel
    .update({ _id : req.body._id}, { $set: { 
        user_first_name     : req.body.user_first_name,
        user_middle_name    : req.body.user_middle_name,
        user_last_name      : req.body.user_last_name,
        user_email          : req.body.user_email,
        user_role           : req.body.user_role

    }}, function(err, done) {
        if (err)
            return res.json(err);
        return  res.json(done);
    });
};

// update user details
module.exports.deleteUser = function(req, res) {
    // console.log('List ID:', req.body._id);
    UsersModel
    .update( { _id : req.body._id} , { $set: { user_status : 'deleted' }}, function(err, done){
        if(err) 
            return res.json(err);
        return res.json(done);
    });
};
