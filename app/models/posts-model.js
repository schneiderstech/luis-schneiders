var mongoose =  require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var postsSchema = new Schema ({ 
    post_inserted_by: { type: ObjectId, ref: 'lfs_users'}, 
    post_author: { type: String}, 
    post_title: { type: String}, 
    post_content: [{ type: String}], 
    post_keywords: { type: String}, 
    post_description: { type: String}, 
    post_media: { type: String},
    post_status: { type: String}, 
    post_url: { type: String}, 
    post_date: { type : Date, default: Date.now }
});

module.exports = mongoose.model('lfs_posts', postsSchema);