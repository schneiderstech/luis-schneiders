// var express         = require('express');
// var req             = require('request');
var async           = require('async');
var path            = require('path');
var fs              = require('fs');

// get POST model
var PostsModel = require ('../models/posts-model');

// get USER model
// var UsersModel = require ('../models/users-model');

//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
var myPort = 8080;

function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}

// read file header.json
var contentHeader = fs.readFileSync(path.join(__dirname, './../../app/models/header.json')),
    contentHeader = JSON.parse(contentHeader);

module.exports.allPublishedPosts = function(req, res) {
    PostsModel
    .find({ post_status: 'publish'})
    .select({ _id: 0, post_title: 1, post_content: 1, post_media: 1, post_url: 1, post_date: 1}) // select fields to display
    .sort({'post_date': 'descending'})
    .exec(function(err, listPublishedPost) {
        if (err) {
            res.render('pages/404.ejs', {
                title: '404: File Not Found',
                contentSEO:
                {
                    mySite: getHostName(req, myPort).hostName,
                    currentPage: '/',
                    title: 'No post was found'
                }
            });
            return err;
        }
        else {
            if (!listPublishedPost.length) {
                res.render('pages/404.ejs', {
                    title: '404: File Not Found',
                    contentSEO:
                    {
                        mySite: getHostName(req, myPort).hostName,
                        currentPage: '/',
                        title: 'No post was found'
                    }
                });  
            } else {
                res.render('pages/blog', {
                    contentHeader: contentHeader,
                    contentSEO:
                    {
                        mySite: getHostName(req, myPort).hostName,
                        currentPage: req.path,
                        title: 'Blog'
                    },
                    listAllPosts: listPublishedPost
                });
            }
        }
    });
};

module.exports.listPostByID = function(req, res) {
    // get ID selected 
    var id = req.params.id;

    async.parallel({
        findPostByID: function(callback){
            PostsModel
            .find({ post_url: id, post_status: 'publish' }, callback)
            .select({ _id: 0, post_title: 1, post_content: 1, post_keywords: 1, post_description: 1, post_media: 1, post_url: 1, post_date: 1}); // select fields to display  
            return callback;          
        },
        findRecentPosts: function(callback){
            PostsModel
            .find({ post_status: 'publish'}, callback)
            .select({ _id: 0, post_title: 1, post_url: 1}) // select fields to display
            .sort({'post_date': 'descending'})
            .limit(5);
            return callback;
        }
    },
    function(err, listPostID) {
        if (err) {
            res.render('pages/post', {
                contentHeader: contentHeader,
                contentSEO:
                {
                    mySite: getHostName(req, myPort).hostName,
                    currentPage: req.path,
                    title: 'Blog'
                },
                listPostByID: 'No post has been found!'
            });
            return err;
        }
        else {
            if (!listPostID.findPostByID.length) {
                // if post was not found return 404
                res.render('pages/404.ejs', {
                    title: '404: File Not Found',
                    contentSEO:
                    {
                        mySite: getHostName(req, myPort).hostName,
                        currentPage: '/',
                        title: 'Node.js developer in Melbourne'
                    }
                });
            } else {
                res.render('pages/post', {
                    contentHeader: contentHeader,
                    contentSEO:
                    {
                        mySite: getHostName(req, myPort).hostName,
                        currentPage: req.path,
                        title: listPostID.findPostByID[0].post_title
                    },
                    listPostByID: listPostID.findPostByID,
                    listRecentPosts: listPostID.findRecentPosts
                });
            }
        }
    });
};

// list all posts for user logged in admin area
module.exports.listAllPosts = function(req, res) {
    // console.log('passport user is:', req.user.user_role);
    // list all posts when is admin
    if (req.user.user_role == 'admin') {
        PostsModel
        .find({ 'post_status' : { $ne : 'deleted' }})
        .sort({ 'post_date': 'ascending' })
        .exec(function(err, listallposts) {
            if (err) {
                return err;
            } else {
                res.json(listallposts);
            }
        });
    } else {
        PostsModel
        .find({ 'post_status' : { $ne : 'deleted' }})
        .where( 'post_inserted_by').equals(req.session.passport.user)
        .sort({ 'post_date': 'ascending' })
        .exec(function(err, listallposts) {
            if (err) {
                return err;
            } else {
                res.json(listallposts);
            }
        });
    }
};

// update post status
module.exports.updatePostStatus = function(req, res) {
    PostsModel
    .update( { _id : req.body.id} , { $set: { post_status : req.body.status }}, function(err, done){
        if(err) 
            return res.json(err);
        return res.json(done);
    });
};
