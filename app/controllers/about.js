var express = require('express');
// var req = require('request');
var path = require('path');
var fs = require('fs');
var router = express();


//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
var myPort = 8080;

function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}

// //  read file header.json
var contentHeader = fs.readFileSync(path.join(__dirname, './../../app/models/header.json')),
    contentHeader = JSON.parse(contentHeader);

// about page 
router.get('/about', function (req, res) {
    'use strict';
    res.render('pages/about', {
        contentHeader: contentHeader,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Skills and work experience'
        }
    });
});

module.exports = router;