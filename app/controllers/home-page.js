var express = require('express');
var path = require('path');
var fs = require('fs');

var router = express();

var myPort = 8080;

//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}

//  read file header.json
var contentHeader = fs.readFileSync(path.join(__dirname, './../../app/models/header.json')),
    contentHeader = JSON.parse(contentHeader);


router.get('/', function (req, res) {
    var contentIndex = fs.readFileSync(path.join(__dirname, './../../app/models/index.json'));
    contentIndex = JSON.parse(contentIndex);
    res.render('pages/index', {
        contentHeader: contentHeader,
        contentIndex: contentIndex,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Node.js developer in Melbourne'
        }
    });
});

module.exports = router;