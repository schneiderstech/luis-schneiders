var express         = require('express');
var req             = require('request');
var path            = require('path');
var fs              = require('fs');
var nodemailer      = require('nodemailer');
var xoauth2         = require('xoauth2');
var bodyParser      = require('body-parser');
var validator       = require('email-validator');

var router = express();


//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
var myPort = 8080;

function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}

// //  read file header.json
var contentHeader = fs.readFileSync(path.join(__dirname, './../../app/models/header.json')),
    contentHeader = JSON.parse(contentHeader);

router.use(bodyParser.urlencoded({ extended: true }));

// get in touch page 
router.get('/get-in-touch', function (req, res) {
    res.render('pages/get-in-touch', {
        contentHeader: contentHeader,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Get in touch'
        },
        emailConfig:
        {
            msg: 'All fields are required!',
            status : '',
            displayClass: 'hide'
        }
    });
});

// post contact form
router.post('/get-in-touch', function (req, res) {
    // honeypot for spambots
    if(req.body.company){
        res.render('pages/get-in-touch', {
            contentHeader: contentHeader,
            contentSEO:
            {
                mySite: getHostName(req, myPort).hostName,
                currentPage: req.path,
                title: 'Get in touch'
            },
            err: true,
            page: 'pages/get-in-touch',
            type: 'empty',
            name: req.body.name,
            email: req.body.email,
            subject: req.body.subject,
            message: req.body.message,
            emailConfig:
            {
                msg: 'Spambot detected. Contact us by email',
                status: '',
                displayClass: 'alert '
            }

        });
        return;
    }
    // check if all fields are filled
    if(!req.body.name || !req.body.email || !req.body.subject || !req.body.message){
        // console.log('Im here');
        res.render('pages/get-in-touch', {
            contentHeader: contentHeader,
            contentSEO:
            {
                mySite: getHostName(req, myPort).hostName,
                currentPage: req.path,
                title: 'Get in touch'
            },
            err: true,
            page: 'pages/get-in-touch',
            type: 'empty',
            name: req.body.name,
            email: req.body.email,
            subject: req.body.subject,
            message: req.body.message,
            emailConfig:
            {
                msg: 'All Fields are required!',
                status: '',
                displayClass: 'alert '
            }
        });
        return;
    }
    // check for valid email
    var email_check = validator.validate(req.body.email);
    if(!email_check) {
        console.log('Wrong email format!');
        res.render('pages/get-in-touch', {
            contentHeader: contentHeader,
            contentSEO:
            {
                mySite: getHostName(req, myPort).hostName,
                currentPage: req.path,
                title: 'Get in touch'
            },
            err: true,
            page: 'pages/get-in-touch',
            type: 'empty',
            name: req.body.name,
            email: req.body.email,
            subject: req.body.subject,
            message: req.body.message,
            emailConfig:
            {
                msg: 'Email is incorrect!',
                status: '',
                displayClass: 'alert '
            }
        });
        return;
    }
    var transporter = nodemailer.createTransport('direct:?name=hostname');
    // var transporter = nodemailer.createTransport({
    //     service: 'gmail',
    //     auth: {
    //         xoauth2: xoauth2.createXOAuth2Generator({
    //             user: 'Schneiders Tech',
    //             clientId: '578289351272-egp54vb8fsep0sfc6oj4mdkoeqq1b0iu.apps.googleusercontent.com&access_type=offline',
    //             clientSecret: 'htJkG6midAaZ67WMRWsCRCGl',
    //             https://accounts.google.com/o/oauth2/revoke
    //         })
    //     }
    // });

    // Fill mail options
    var mailOptions = {
        from: req.body.name +  '<' + req.body.email + '>', // sender address
        to: 'schneiders.tech@gmail.com',
        subject: 'Contact via nodejs website', // Subject line
        text: 'Contact via Node.js website', // plaintext body
        html: 'From: <b>'+ req.body.name + '</b><br> <p>Subject: <b>' + req.body.message + '</b></p>'
    };
    transporter.sendMail(mailOptions, function(error, info){
        // email not sent
        if(error){
            console.log('Error is: ', error);
            res.render('pages/get-in-touch', {
                contentHeader: contentHeader,
                contentSEO:
                {
                    mySite: getHostName(req, myPort).hostName,
                    currentPage: req.path,
                    title: 'Get in touch'
                },
                err: true,
                page: 'pages/get-in-touch',
                type: 'error',
                emailConfig:
                {
                    msg : 'All fields are required!',
                    status: 'Error sending email, please try again!',
                    displayClass: 'alert '
                }
            });
        }else{
            res.render('pages/get-in-touch', {
                contentHeader: contentHeader,
                contentSEO:
                {
                    mySite: getHostName(req, myPort).hostName,
                    currentPage: req.path,
                    title: 'Get in touch'
                },
                err: true,
                page: 'pages/get-in-touch',
                type: 'success',
                emailConfig:
                {
                    msg : 'All fields are required!',
                    status: 'Email sent successfuly!',
                    displayClass: 'success'
                }
            });
        }
    });
});

module.exports = router;
