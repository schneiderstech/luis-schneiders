var express         = require('express');
var passport        = require('passport');
var router          = express();
var isLoggedIn      = require('../../config/auth');

router.get('/admin/main', isLoggedIn, function(req, res) {
    res.render('admin/main.html', {
        title : 'Admin Dashboard',
        userLogged : req.user
    });
});

router.get('/admin/logout', function(req, res) {
    req.logout();
    res.redirect('/admin/login');
});

// route middleware to make sure
 function isLoggedIn(req, res, next) {

     // // if user is authenticated in the session, carry on
     if (req.isAuthenticated())
         return next();

     // // if they aren't redirect them to the home page
     res.redirect('/admin/login');
 }

module.exports = router;