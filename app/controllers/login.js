var express         = require('express');
var passport        = require('passport');
var router          = express();

var api             = require('./../models/users-manager' );

router.get('/admin/login', api.displayRegisterPage );

// process the login form
router.post('/admin/login', passport.authenticate('local-login', {
    successRedirect : '/admin/main', // redirect to the secure profile section
    failureRedirect : '/admin/login', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));

// process to register first user
router.post('/admin/register', passport.authenticate('local-signup', {
    successRedirect : '/admin/main', // redirect to the secure profile section
    failureRedirect : '/admin/register', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));

module.exports = router;