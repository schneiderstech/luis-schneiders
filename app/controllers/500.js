var express = require('express');
var router = express();

var myPort = 8080;

//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}

// Handle 500
router.use(function (error, req, res) {
    res.status(500);
    res.render('pages/500.ejs', {
        title: '500: Internal Server Error',
        error: error,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: '/',
            title: 'Node.js developer in Melbourne'
        }
        
    });
});

module.exports = router;