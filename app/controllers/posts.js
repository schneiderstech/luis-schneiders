var express         = require('express');
var req             = require('request');
var bodyParser      = require('body-parser');
var router          = express();
var isLoggedIn      = require('../../config/auth');

router.use(bodyParser.json());

var api = require('./../models/posts-manager' );

router.get('/blog', api.allPublishedPosts );

router.get('/blog/:id', api.listPostByID );

router.get('/listallposts', isLoggedIn, api.listAllPosts);

router.put('/updatepoststatus', isLoggedIn, api.updatePostStatus);

module.exports = router;