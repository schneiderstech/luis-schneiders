var express = require('express');
var req = require('request');
var path = require('path');
var fs = require('fs');
var router = express();


//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
var myPort = 8080;

function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}

// //  read file header.json
var contentHeader = fs.readFileSync(path.join(__dirname, './../../app/models/header.json')),
    contentHeader = JSON.parse(contentHeader);
var contentProjects = fs.readFileSync(path.join(__dirname, './../../app/models/projects.json')),
    contentProjects = JSON.parse(contentProjects);

// projects page 
router.get('/projects', function (req, res) {
    'use strict';
    res.render('pages/projects', {
        contentHeader: contentHeader,
        contentProjects: contentProjects,
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: req.path,
            title: 'Check out some awesome projects'
        }
    });
});

module.exports = router;