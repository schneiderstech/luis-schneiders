var express         = require('express');
var router          = express();

router.get('/admin/new-password', function(req, res) {
    res.render('admin/new-password.html', {
        title: 'Login',
        message : req.flash('loginMessage')
    });
});

module.exports = router;