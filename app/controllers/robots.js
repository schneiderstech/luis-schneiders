var express         = require('express');
var path            = require('path');

var router = express();

router.get('/robots.txt', function(req, res) {
    res.setHeader('Content-Type', 'text/txt');
    res.sendFile(path.join(__dirname, '../../views/pages/robots.txt'));
});

module.exports = router;