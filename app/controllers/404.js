var express = require('express');
var router = express();

var myPort = 8080;

//  to get http://localhost:8080 or https://nodejs.luisschneiders.com path
function getHostName(req, myPort){
    return{
        hostName: 'http://'+req.hostname+':'+myPort
    };
}

// Handle 404
router.use(function (req, res) {
    res.status(404);
    res.render('pages/404.ejs', {
        title: '404: File Not Found',
        contentSEO:
        {
            mySite: getHostName(req, myPort).hostName,
            currentPage: '/',
            title: 'Node.js developer in Melbourne'
        }
    });
});

module.exports = router;