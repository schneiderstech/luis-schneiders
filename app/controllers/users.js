var express         = require('express');
var bodyParser      = require('body-parser');
var router          = express();
var multer          = require('multer');
var isLoggedIn      = require('../../config/auth');

var api = require('./../models/users-manager' );

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.get('/listallusers', isLoggedIn, api.listAllUsers);

router.post('/addnewuser', isLoggedIn, api.addNewUser);

router.put('/updateuserstatus', isLoggedIn, api.updateUserStatus);

router.get('/getuser/:id', isLoggedIn, api.getUserById);

router.put('/updateuserdetails', isLoggedIn, api.updateUserDetails);

router.post('/uploads', multer({dest: './uploads/'}).single('user_image'), api.uploadProfile);
// router.post('/uploads', api.uploadProfile);

router.put('/deleteuser', isLoggedIn, api.deleteUser);

module.exports = router;