var express = require('express');
var sm = require('sitemap');
var router = express();
// create sitemap
var mySite = 'http://local.developing:8080';
var sitemap = sm.createSitemap({
    hostname: mySite,
    cacheTime: 1000 * 60 * 24,        // keep sitemap cached for 24 hours
    urls: [
        { url: '/', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/index.ejs' },
        { url: '/about', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/about.ejs' },
        { url: '/get-in-touch', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/get-in-touch.ejs' },
        { url: '/projects', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/projects.ejs' },
        { url: '/blog', changefreq: 'monthly', priority: 0.8, lastmodrealtime: true, lastmodfile: 'views/pages/blog.ejs' }
    ]
});

// sitemap page
router.get('/sitemap.xml', function (req, res) {
    sitemap.toXML(function (err, xml) {
        if (err) {
            return res.status(500).end();
        }
        res.header('Content-Type', 'application/xml');
        res.send(xml);
    });
});

module.exports = router;