'use strict';

angular.module('adminApp.roles', [])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    // route for list all users page
    .when('/roles', {
        templateUrl : '/settings/roles.html',
        controller  : 'RolesCtrl'
    });
}])
.controller('RolesCtrl', function($scope, $http, $sce) {
    var refresh = function() {
        $http({
            method  : 'GET',
            url     : '/listallroles'
        })
        .then(function successCallback(response){
            $scope.listallroles = response;
            //  trustAsHtml 
            $scope.$sce = $sce;

            $scope.pageSettings = {
                title               : 'Roles',
                icon                : 'fa fa-sliders admin-title'
            };

            $scope.listallroles = [
                { 'role' : 'admin' },
                { 'role' : 'author' },
                { 'role' : 'editor' }
            ];

            $scope.listallstatus = [
                { 'status' : 'active' },
                { 'status' : 'inactive' }
            ];

            // apply filters
            $scope.predicate = 'status';
            $scope.reverse = true;
            $scope.order = function(predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };
        }, function errorCallback(response){

        });
    };

    refresh();

    // add user to Mongodb
    // $scope.addNewUser = function () {
    //     $http({
    //         method  : 'POST',
    //         url     : '/addnewuser',
    //         data    : $scope.user
    //     })
    //     .then(function successCallback(response){
    //         if (response.data.code === 11000) {
    //             refresh();
    //             $scope.message      = 'User already exist!';
    //             $scope.showClass    = 'alert show-all';
    //         }
    //         else if (response.data.name == 'ValidationError'){
    //             refresh(); 
    //             $scope.message      = 'Server error, try again please!';
    //             $scope.showClass    = 'alert show-all';
    //         }
    //         else{
    //             refresh(); 
    //             $scope.message      = 'User has been created!';
    //             $scope.showClass    = 'success show-all';
    //         }

    //     }, function errorCallback(response){
    //         refresh(); 
    //         $scope.message      = 'Server error, try again please!';
    //         $scope.showClass    = 'alert show-all';
    //     });
    // };

    // update user status
    // $scope.updateStatus = function(id, status){
    //     // ng-change does not get the value after the change has been made, thus we need to invert the values  
    //     if(status == 'inactive'){
    //         status = 'active';
    //     }else{
    //         status = 'inactive';
    //     }
    //     $http({
    //         method  : 'PUT',
    //         url     : '/updateuserstatus',
    //         data    : {
    //             id      : id,
    //             status  : status
    //         }
    //     })
    //     .then(function successCallback(response){
    //         // return server response
    //     }, function errorCallback(response){
    //         refresh(); 
    //         $scope.message      = 'Server error, try again please!';
    //         $scope.showClass    = 'alert show-all';
    //     });
    // };
});
