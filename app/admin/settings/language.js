'use strict';

var angular;

angular.module('adminApp.language', [])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/language', {
        templateUrl : '/settings/language.html',
        controller  : 'Language'
    });
}])
.controller('Language', function($scope, $http, $sce) {
    //  trustAsHtml 
    $scope.$sce = $sce;
    $scope.pageSettings = {
        icon : 'fa fa-language admin-title'
    };
})
.directive('localeSelector', function($translate) {
    return {
        restrict: 'C',
        replace: true,
        templateUrl: '/settings/locale-selector.html',
        link: function(scope) {
            // Get active locale even if not loaded yet:
            scope.locale = $translate.proposedLanguage();
            scope.setLocale = function() {
                $translate.use(scope.locale);
            };
        }
    };
});
