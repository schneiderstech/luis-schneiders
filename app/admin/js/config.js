'use strict';
// Declare app level module which depends on views, and components
var angular;

angular.module('adminApp', [
    'ngRoute', 
    'ngSanitize',
    'ngAnimate',
    'ngCookies',
    'pascalprecht.translate',
    'ngFileUpload',
    'adminApp.main',
    'adminApp.language',
    'adminApp.users',
    'adminApp.posts',
    'adminApp.dashboardfeeds',
    'adminApp.roles'
])
.config(function($locationProvider, $routeProvider, $translateProvider){    
    $routeProvider.otherwise({redirectTo: '/'});

    $translateProvider.useCookieStorage();
    $translateProvider.useUrlLoader('/api/lang');    
    $translateProvider.preferredLanguage('en');
    // Enable escaping of HTML
    $translateProvider.useSanitizeValueStrategy('escape');

});
