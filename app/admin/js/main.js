'use strict';

var angular;

angular.module('adminApp.main', [])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    // route for the main page
    .when('/', {
        templateUrl : '/menu/dashboard.html',
        controller  : 'MainCtrl'
    });
}])
.controller('MainCtrl', function($scope) {
    $scope.template = 'Home Page';
});
