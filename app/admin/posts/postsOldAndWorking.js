'use strict';

var angular;

angular.module('adminApp.posts', [])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    // route for to find all posts
    .when('/list-all-posts', {
        templateUrl : '/posts/find-all-posts.html',
        controller  : 'FindAllPostsCtrl'
    })
    // route for the new post
    .when('/new-post', {
        templateUrl : '/posts/new-post.html',
        controller  : 'NewPostCtrl'
    });
}])
.controller('FindAllPostsCtrl', function($scope, $http, $sce) {
    var refresh = function() {

        $http({
            method  : 'GET',
            url     : '/listallposts'
        })
        .then(function successCallback(response){
            $scope.listallpost = response; 

            //  trustAsHtml 
            $scope.$sce = $sce;
            $scope.pageSettings = {
                icon : 'fa fa-thumb-tack admin-title'
            };

            // apply filters
            $scope.predicate = 'date';
            $scope.reverse = true;
            $scope.order = function(predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };
            
        }, function errorCallback(response){

        });

    };
    
    refresh();

    // update post status
    $scope.updateStatus = function(id, status){
        // ng-change does not get the value after the change has been made, thus we need to invert the values  
        if(status == 'draft'){
            status = 'publish';
        }else{
            status = 'draft';
        }
        $http({
            method  : 'PUT',
            url     : '/updatepoststatus',
            data    : {
                id      : id,
                status  : status
            }
        })
        .then(function successCallback(response){
            // return server response
        }, function errorCallback(response){
            refresh(); 
            $scope.message      = 'Server error, try again please!';
            $scope.showClass    = 'alert show-all';
        });
    };
})
.controller('NewPostCtrl', function($scope, $http, $sce) {
    //  trustAsHtml 
    $scope.$sce = $sce;
    $scope.pageSettings = {
        icon : 'fa fa-thumb-tack admin-title'
    };
    $scope.formDescription = {

    };

});