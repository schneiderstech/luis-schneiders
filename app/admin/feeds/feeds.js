'use strict';

var angular;

angular.module('adminApp.dashboardfeeds', [])
.controller('FeedCtrl', ['$scope', 'FeedService', function($scope, Feed) {
    $scope.loadButtonText = '';
    $scope.loadFeed=function(e){        
        Feed.parseFeed($scope.feedSrc).then(function(res){
            $scope.loadButtonText = angular.element(e.target).text();
            $scope.feeds = res.data.responseData.feed.entries;
        });
    };
}])
.factory('FeedService',['$http', function($http) {
    return {
        parseFeed : function(url){
            return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
        }
    };
}]);