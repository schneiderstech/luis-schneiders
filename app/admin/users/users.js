'use strict';

var angular;

angular.module('adminApp.users', [])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    // route for list all users page
    .when('/findAllUsers', {
        templateUrl : '/users/find-all-users.html',
        controller  : 'FindAllUsersCtrl'
    })
    .when('/getuser/:id', {
        templateUrl : '/users/edit-user.html',
        controller  : 'EditUserCtrl'        
    });
}])
.controller('FindAllUsersCtrl', function($scope, $http, $sce) {
    var refresh = function() {
        $http({
            method  : 'GET',
            url     : '/listallusers'
        })
        .then(function successCallback(response){
            $scope.listallusers = response;
            $scope.user         = '';
            //  trustAsHtml 
            $scope.$sce = $sce;

            $scope.pageSettings = {
                icon                : 'fa fa-user admin-title'
            };

            $scope.formCtrl = {
                username            : 'username',
                email               : 'email',
                role                : 'role',
                status              : 'status'
            };

            $scope.listallroles = [
                { 'role' : 'admin' },
                { 'role' : 'author' },
                { 'role' : 'editor' }
            ];

            $scope.listallstatus = [
                { 'status' : 'active' },
                { 'status' : 'inactive' }
            ];

            // apply filters
            $scope.predicate = 'status';
            $scope.reverse = true;
            $scope.order = function(predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };
        }, function errorCallback(response){
            $scope.message = response.status;
            $scope.showClass    = 'alert show-all';
        });
    };

    refresh();

    // add user to Mongodb
    $scope.addNewUser = function () {
        // check to make sure the form is completely valid
        if ($scope.formAddUser.$valid) {
            $http({
                method  : 'POST',
                url     : '/addnewuser',
                data    : $scope.user
            })
            .then(function successCallback(response){
                // console.log('Response: ', response);
                if (response.data.code === 11000) {
                    refresh();
                    $scope.message      = 'User already exist!';
                    $scope.showClass    = 'alert show-all';
                }
                else if (response.data.name == 'ValidationError'){
                    refresh(); 
                    $scope.message      = 'All fields are required!';
                    $scope.showClass    = 'alert show-all';
                }
                else{
                    refresh(); 
                    $scope.message      = 'User has been created!';
                    $scope.showClass    = 'success show-all';
                }

            }, function errorCallback(response){
                refresh(); 
                $scope.message      = 'Server error:' + response.status;
                $scope.showClass    = 'alert show-all';
            });
        } else {
            $scope.message      = 'All fields are required!';
            $scope.showClass    = 'alert show-all';
        }
    };

    // update user status
    $scope.updateStatus = function(id, status){
        // ng-change does not get the value after the change has been made, thus we need to invert the values  
        if(status == 'inactive'){
            status = 'active';
        }else{
            status = 'inactive';
        }
        $http({
            method  : 'PUT',
            url     : '/updateuserstatus',
            data    : {
                id      : id,
                status  : status
            }
        })
        .then(function successCallback(){
            // return server response
        }, function errorCallback(response){
            refresh(); 
            $scope.message      = 'Server error: ' + response.status;
            $scope.showClass    = 'alert show-all';
        });
    };

})
.controller('EditUserCtrl', function($scope, Upload, $http, $sce, $location, $timeout) {
    $http({
        method  : 'GET',
        url     : $location.$$path
    })
    .then(function successCallback(response){
        // when user's status is: DELETED        
        if (response.data == null) {
            $location.path('/findAllUsers');
        } else {
            $scope.listuserdetails = response;
            $scope.pageSettings = {
                icon                : 'fa fa-user admin-title'
            };
            $scope.listallroles = [
                { 'role' : $scope.listuserdetails.data.user_role },
                { 'role' : 'admin' },
                { 'role' : 'author' },
                { 'role' : 'editor' }
            ];
            // button update user
            $scope.updateUser = function() {
                // check to make sure the form is completely valid
                if ($scope.userForm.$valid) {
// old but working
                    $http({
                        method  : 'PUT',
                        url     : '/updateuserdetails',
                        data    :  $scope.listuserdetails.data
                    })
                    .then(function successCallback(response){
                        if (response.data.name == 'ValidationError'){
                            $scope.message      = 'All fields are required!';
                            $scope.showClass    = 'alert show-all';
                        }
                        else{
                            $scope.message      = 'User has been updated!';
                            $scope.showClass    = 'success show-all';
                        }
                    }, function errorCallback(response){
                        $scope.message      = 'Server error: ' + response.status;
                        $scope.showClass    = 'alert show-all';
                    });
// old but working                    
                }

            };
            $scope.uploadPicture = function (user_image) {
                console.log('File passed by Angular: ', user_image);
                $http({
                    method  : 'POST',
                    url     : '/uploads',
                    data    : {
                        'user_image': user_image
                    },
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (resp) {
                    console.log('Success ' + resp.config.data.name + ' uploaded. Response: ' + resp.data);
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    user_image.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    console.log('progress: ' + user_image.progress + '% ' + evt.config.data.name);
                });
            };            

            // button delete user
            $scope.deleteUser = function () {
                $http({
                    method  : 'PUT',
                    url     : '/deleteuser',
                    data    : $scope.listuserdetails.data
                })
                .then(function successCallback(){
                    $scope.message      = 'User has been deleted!';
                    $scope.showClass    = 'success show-all';
                    $timeout(redirectToAllUsers, 2000);
                    
                }, function errorCallback(response){
                    $scope.message      = 'Server error: ' + response.status;
                    $scope.showClass    = 'alert show-all';
                });

            };
        }
        function redirectToAllUsers() {
            $location.path('/findAllUsers');
        }

    }, function errorCallback(response){
        // refresh(); 
        $scope.message      = 'Server error: ' + response.status;
        $scope.showClass    = 'alert show-all';
    });
});
