# README # (simplified)

This is a CMS project developed using Node.js and Angular v1.5 

It has multi language features, that means that the user will be able to select which language he/she wants to use.

### How do I get set up? ###

* clone the repository luis-schneiders
* run: **npm install**
* run: **npm install mongoose** to install mongoose, for more info: [Installing Mongoose](https://www.npmjs.com/package/mongoose)

### Setup AWS account ###

For a complete guidelines of how to setup your AWS, please visit: 
[AWS Amazon](https://aws.amazon.com)