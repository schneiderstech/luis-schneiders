// config/passport.js

// load all the things we need
var express         = require('express');
var LocalStrategy   = require('passport-local').Strategy;
var router          = express();

// load up the user model
var User       		= require('../app/models/users-model');

// expose this function to our app using module.exports
module.exports = function(passport) {
	// =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user, user_role, user_name) {
            done(err, user, user_role, user_name);
        })
        .select({ user_role : 1, user_name : 1});
    });

	// =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with username
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done) {

		// find a user whose username is the same as the forms username
		// we are checking to see if the user trying to login already exists
        User.find({ user_name :  username }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);
            // check to see if theres already a user with that username
            if (user.length > 0) {
                return done(null, false, req.flash('signupMessage', 'That username is already taken.'));
            } else {
				// if there is no user with that username
                // create the user
                var newUser = new User();

                // set the user's local credentials
                newUser.user_name           = username;
                newUser.user_first_name     = '';
                newUser.user_middle_name    = '';
                newUser.user_last_name      = '';
                newUser.user_email          = req.body.email;
                newUser.user_password       = newUser.generateHash(password); // use the generateHash function in our user model
                newUser.user_role           = 'admin';
                newUser.user_status         = 'active';

				// save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }
        });
    }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with username
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done) { // callback with username and password from our form
        // find a user whose username is the same as the forms username
        // we are checking to see if the user trying to login already exists
        User.findOne({ user_name : username, user_status : 'active' }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found or active!')); // req.flash is the way to set flashdata using connect-flash

            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'User and password does not match!')); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, user);
        });
    }));
};