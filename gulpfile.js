/*!
 * gulp
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del');

// Styles
gulp.task('styles', function() {
    return gulp.src('./public/css/app.css')
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('./public/min/styles'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(cssnano())
        .pipe(gulp.dest('./public/min/styles'))
        .pipe(notify({ message: 'Styles task complete' }));
});

// Scripts
// gulp.task('scripts', function() {
//     return gulp.src('src/scripts/**/*.js')
//         .pipe(jshint('.jshintrc'))
//         .pipe(jshint.reporter('default'))
//         .pipe(concat('main.js'))
//         .pipe(gulp.dest('dist/scripts'))
//         .pipe(rename({ suffix: '.min' }))
//         .pipe(uglify())
//         .pipe(gulp.dest('dist/scripts'))
//         .pipe(notify({ message: 'Scripts task complete' }));
// });

// Images
// gulp.task('images', function() {
//     return gulp.src('src/images/**/*')
//         .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
//         .pipe(gulp.dest('dist/images'))
//         .pipe(notify({ message: 'Images task complete' }));
// });

// Clean
gulp.task('clean', function() {
    return del(['./public/css']);
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('styles');
});

// Watch
gulp.task('watch', function() {

    // Watch .scss files
    gulp.watch('./public/css/app.css', ['styles']);

    // Watch .js files
    // gulp.watch('public/js/*.js', ['scripts']);

    // Watch image files
    // gulp.watch('public/img/*', ['images']);

    // Create LiveReload server
    // livereload.listen();

    // Watch any files in dist/, reload on change
    gulp.watch(['./public/**']).on('change', livereload.changed);

});
